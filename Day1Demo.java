import java.util.*;
import java.util.Arrays;
import java.util.Scanner;
public class Day1Demo
{
 public static void main(String[] args)
 {
  //prints the name on the output screen
  System.out.println("MY NAME IS SRINIJA");
  //defining array of string
  String[] tasks={"wake up","eat","eat","sleep","read"};
  //stores the tasks in the string
  System.out.println("assigned tasks are:"+Arrays.toString(tasks));
  //sorts array in alphabetical order or ascending order
  Arrays.sort(tasks);
  System.out.println("Ascending order:"+Arrays.toString(tasks));
  // sorts array in descending order
  Arrays.sort(tasks,Collections.reverseOrder());
  System.out.println("Descending order:"+Arrays.toString(tasks));
  for(int i=0;i<tasks.length;i++)
  {
   for(int j=i+1;j<tasks.length;j++)
   {
    if(tasks[i]==tasks[j])
    {
     System.out.println("repeated task is:"+tasks[j]);
    }
   }
  }
 }
}
