package com;
import java.util.Stack;
import java.util.Iterator;
public class Week2Day1Code3 {

	public static void main(String[] args) {
		Stack<String> stack=new Stack<>();
		stack.push("Vishwa");
		stack.push("Raj");
		stack.push("srinija");
		stack.push("srimukhi");
		stack.push("mom");
		System.out.println("stack after adding:"+stack);
		System.out.println("stack after using iterator");
		Iterator<String> itr=stack.iterator();
		while(itr.hasNext())
		{
			System.out.println(itr.next()+"");
		}
		System.out.println();
		
	}

}
