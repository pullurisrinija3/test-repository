class Account

{
    String name;
    int accno;
        Account(String name, int accno)

        {
          this.name=name;
          this.accno=accno;
         }

        void display()

       {    
          System.out.println("name="+name);
          System.out.println("accno="+accno);
        }

}

class CurrentAccount extends Account

{

        int currentBalance;

        CurrentAccount(String a, int b, int c)

        {

                super(a,b);

                currentBalance=c;

        }

        void display()

        {

                super.display();

                System.out.println ("Current Balance: "+currentBalance);

        }

}

class AccountDetails extends CurrentAccount

{
         int depoAmt,withAmt;

      AccountDetails(String a, int b, int c, int d, int e)

        {
          super(a,b,c);
          depoAmt=d;
          withAmt=e;

         }

        void display()

        {        super.display();
                 System.out.println ("Deposited amount:"+depoAmt);
                 System.out.println ("Withdrawn amount:"+withAmt);
        }

}

class Day4Code2

{

        public static void main(String args[])

        {

         AccountDetails A = new AccountDetails("Harshit",11111,10000,5000,500);

                A.display();

        }

}

 