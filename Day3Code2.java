import java.util.*;
class Vehicle
{
 void noOfWheels()
 {
  System.out.println("no.of wheels for vehicle=undefined");
 }
}
class Scooter extends Vehicle
{
 void noOfWheels()
 {
  System.out.println("no.of wheels for Scooter =2");
 }
} 
class Car extends Vehicle
{
 void noOfWheels()
 {
  System.out.println("no.of wheels for car=4");
 }
}
class Day3Code2
{
 public static void main(String[] args)
 {
  Vehicle vehicle = new Vehicle();
  Vehicle scooter = new Scooter();
  Vehicle car = new Car(); 
   vehicle.noOfWheels();     
   scooter.noOfWheels();
   car.noOfWheels(); 
 }
}        