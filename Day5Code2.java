import java.util.Scanner;
public class Day5Code2
{
  public static void main(String[] args)
{
       Scanner sc=new Scanner(System.in);
       int a=sc.nextInt();
       int b=sc.nextInt();
       System.out.println("values before swapping");
       System.out.println("a="+a);
       System.out.println("b="+b);
       System.out.println("values after swapping");
	int temp;
	temp=a;
	a=b;
        b=temp;
	System.out.println("a="+a);
	System.out.println("b="+b);
}
}
